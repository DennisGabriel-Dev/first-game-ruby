# Dependencies gosu(required)
## Dependencies for both C++ and Ruby
```sudo apt-get install build-essential libsdl2-dev libgl1-mesa-dev libgmp-dev libfontconfig1-dev```

## To install Ruby itself - if you are using rvm or rbenv, please skip this step
```sudo apt-get install ruby-dev```

## Clone the project
```git clone https://gitlab.com/DennisGabriel-Dev/first-game-ruby```

## Open project folder
```cd first-game-ruby```

## Run bundle install
```bundle install```

## Finally, open the game
```ruby game.rb```

### Important note! the game was made in ruby 3.2.2